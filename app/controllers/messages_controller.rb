class MessagesController < ApplicationController

  def new
    @message = Message.new
  end

  def create
    @message = Message.new(params[:message])
    if @message.valid?
      notif_admin_about_new_message(@message)
      redirect_to new_message_path, notice: "Your message has been sent."
    else
      render :new
    end
  end

  def notif_admin_about_new_message(message)
    MessageMailer.new_message(message).deliver_now
  end

end
