module MessagesHelper
  def render_flash_messages
    msg = nil
    class_name = nil
    [:notice, :success, :alert].each do |key|
      next if flash[key].blank?
      msg = flash[key]
      class_name = key
      class_name = :success if %w(notice success).include?(key.to_s)
      break
    end
    return if msg.blank?
    close_link = link_to("x", "#", class: "close")
    msg.blank? ? "" : content_tag(:div, "#{close_link}#{msg}".html_safe, :class => "alert alert-#{class_name}", "data-dismiss" => "alert")
  end
end
