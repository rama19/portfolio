class Message
  include ActiveModel::Model

  attr_accessor :fname, :lname, :email, :phone, :msg

  validates :fname, presence: true
  validates :lname, presence: true
  validates_format_of :email, :with => /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/
  validates :msg, presence: true
end
