$('#twitter').sharrre({
  share: {
    twitter: true
  },
  template: '<a class="box" href="#"><div class="share"><i class="fa fa-twitter"></i></div><div class="count" href="#">{total}</div></a>',
  enableHover: false,
  enableTracking: true,
  buttons: { twitter: {via: '_JulienH'}},
  click: function(api, options){
    api.simulateClick();
    api.openPopup('twitter');
  }
});
$('#facebook').sharrre({
  share: {
    facebook: true
  },
  template: '<a class="box" href="#"><div class="share"><i class="fa fa-facebook"></i></div><div class="count" href="#">{total}</div></a>',
  enableHover: false,
  enableTracking: true,
  click: function(api, options){
    api.simulateClick();
    api.openPopup('facebook');
  }
});
$('#pinterest').sharrre({
  share: {
    pinterest: true
  },
  template: '<a class="box" href="#"><div class="share"><i class="fa fa-pinterest"></i></div><div class="count" href="#">{total}</div></a>',
  enableHover: false,
  enableTracking: true,
  click: function(api, options){
    api.simulateClick();
    api.openPopup('pinterest');
  }
});
