class MessageMailer < ApplicationMailer
  default(
    to: "matijaramic@gmail.com",
    from: "matijaramic@gmail.com"
  )

  def new_message(message)
    @message = message
  end

end
