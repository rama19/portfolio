Rails.application.routes.draw do

  root to: "pages#index"

  %w(about).each do |page|
    get page, to: "pages##{page}", as: page
  end

  get "contact", to: "messages#new", as: :contact
  resources :messages, only: [:create, :new]

end
